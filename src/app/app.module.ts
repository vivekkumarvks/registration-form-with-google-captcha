import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ReavtiveRegFormComponent } from './reavtive-reg-form/reavtive-reg-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import {
  RecaptchaFormsModule,
  RecaptchaModule,
  RECAPTCHA_SETTINGS,
  RecaptchaSettings
} from "ng-recaptcha";

@NgModule({
  declarations: [
    AppComponent,
    ReavtiveRegFormComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
  ],
  providers: [{
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: "6Lf6ankaAAAAAPIyMXjumhxliiQhu8ncaoBmgu9m"
    } as RecaptchaSettings
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
